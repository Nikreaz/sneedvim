"Config by Nikreaz
"https://codeberg.org/nikreaz/sneedvim
"Syntax Highlighting
syntax on
set nocompatible
"Dark background
set background=dark
filetype on
filetype plugin on
filetype indent on
" Numbers on the left
set number
" Plug
call plug#begin('~/.vim/plugged')


  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'morhetz/gruvbox'
  Plug 'preservim/nerdtree'
"Icons
  Plug 'ryanoasis/vim-devicons'

call plug#end()

colorscheme gruvbox
" Airline panel
let g:airline_theme='gruvbox'
" }}}
